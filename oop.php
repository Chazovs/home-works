<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?><?
declare(strict_types=1);

interface primat
{
    public function eat($food);

    public function drink();
}

abstract class human
{
    public function goForWork(int $chislo): string
    {
        echo "иду на работую. мое любимое число: " . $chislo;
    }
}

trait plankton
{
    function drinkCoffee(string $like): string
    {
        echo "я пью " . $like . " кофе";
    }
}


class developer extends human implements primat
{
    use plankton;

    // должность разработчика
    var $devStatus;
    // специализация разработчика
    var $devSpec;

    function __construct($devStatus, $devSpec)
    {
        if ($devSpec!="fornt" || $devSpec!="back") {
            throw new Exception("Специализация может иметь значения только front и back");
        }
        if (gettype($devStatus)!="string") {
            throw new Exception("Должность должна быть строковым значением");
        }
        $this->devSpec=$devSpec;
        $this->devStatus=$devStatus;


    }

    public function eat($food)
    {
        echo "я ем ".$food;
    }

    public function drink()
    {
        echo "я пью";
    }

    public function work()
    {
        echo "моя специализация " . $this->devSpec;
    }
}

try {
    $ivan = new developer("джун", "front");
    $anna = new developer("middle", "back");
    var_dump($ivan);
    var_dump($anna);
} catch (Exception $e) {
    echo "все не норм. А конкретно ". $e->getMessage(). "\n";
}
?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>