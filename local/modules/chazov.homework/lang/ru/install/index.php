<?

$MESS["CHAZOV_HOMEWORK_MODULE_NAME"] = "Учебный модуль для домашки";
$MESS["ACADEMY_D7_MODULE_DESC"] = "Ставит восклицательный знак после названия";
$MESS["ACADEMY_D7_PARTNER_NAME"] = "Сергей Чазов";
$MESS["ACADEMY_D7_PARTNER_URI"] = "http://che.w6p.ru";

$MESS["ACADEMY_D7_DENIED"] = "Доступ закрыт";
$MESS["ACADEMY_D7_READ_COMPONENT"] = "Доступ к компонентам";
$MESS["ACADEMY_D7_WRITE_SETTINGS"] = "Изменение настроек модуля";
$MESS["ACADEMY_D7_FULL"] = "Полный доступ";

$MESS["ACADEMY_D7_INSTALL_TITLE"] = "Установка модуля";
$MESS["ACADEMY_D7_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";

#работа с .settings.php
$MESS["ACADEMY_D7_INSTALL_COUNT"] = "Количество установок модуля: ";
$MESS["ACADEMY_D7_UNINSTALL_COUNT"] = "Количество удалений модуля: ";

$MESS["ACADEMY_D7_NO_CACHE"] = 'Внимание, на сайте выключено кеширование!<br>Возможно замедление в работе модуля.';
#работа с .settings.php
?>