<?php

namespace Chazov\Homework;

class Event
{
    public function eventHandler(&$arFields)
    {
        if (substr($arFields['NAME'], -1) !== '!') {
            $arFields['NAME'] .= "!";
        }
    }
}
