<?php

namespace Chazov\Homework;

use Bitrix\Main;
use Bitrix\Main\Entity;

/**
 * Class ElementPropM16Table
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> IBLOCK_ELEMENT_ID int mandatory
 * <li> IBLOCK_PROPERTY_ID int mandatory
 * <li> VALUE string mandatory
 * <li> VALUE_ENUM int optional
 * <li> VALUE_NUM double optional
 * <li> DESCRIPTION string(255) optional
 * </ul>
 *
 * @package Bitrix\Iblock
 **/
class LinuxSoftM16Table extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_iblock_element_prop_m16';
    }
    
    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return [
            
            new Entity\IntegerField('ID', ['primary' => true, 'autocomplete' => true]),
            new Entity\IntegerField('IBLOCK_ELEMENT_ID', ['required' => true,]),
            new Entity\IntegerField('IBLOCK_PROPERTY_ID', ['required' => true,]),
            new Entity\TextField('VALUE', ['required' => true]),
            new Entity\IntegerField('VALUE_ENUM'),
            new Entity\FloatField('VALUE_NUM'),
            new Entity\StringField('DESCRIPTION', ['validation' => [__CLASS__, 'validateDescription']]),
        ];
    }
    
    /**
     * Returns validators for DESCRIPTION field.
     *
     * @return array
     */
    public static function validateDescription()
    {
        return [
            new Main\Entity\Validator\Length(null, 255),
        ];
    }
}
