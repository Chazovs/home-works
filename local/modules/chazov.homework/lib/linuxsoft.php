<?php

namespace Chazov\Homework;

use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Main\Entity\ReferenceField;

class LinuxSoftTable extends ElementTable
{
    public static function getMap()
    {
        $arMap = parent::getMap();
        
        //простое текстовое поле
        $arMap[] = new ReferenceField('S16', '\Chazov\Homework\LinuxSoftS16', ['=this.ID' => 'ref.IBLOCK_ELEMENT_ID']);
        
        //Имя и фамилия
        $arMap[]      = new ReferenceField('FIO_REF', '\Bitrix\Main\UserTable', ['=this.CREATED_BY' => 'ref.ID']);
        $arMap['FIO'] = new ExpressionField('FIO', "CONCAT(%s, ' ', %s)", ['FIO_REF.NAME', 'FIO_REF.LAST_NAME']);
        
        //Картинка
        $arMap[]          = new ReferenceField('PICTURE_REF', '\Bitrix\Main\FileTable', ['=this.PREVIEW_PICTURE' => 'ref.ID',]);
        $arMap['PICTURE'] = new ExpressionField('PICTURE', "CONCAT(%s, '/', %s)", ['PICTURE_REF.SUBDIR', 'PICTURE_REF.FILE_NAME']);


        //множественные свойства
        $arMap[] = new ReferenceField('M16', '\Chazov\Homework\LinuxSoftM16', ['=this.ID' => 'ref.IBLOCK_ELEMENT_ID', '=ref.IBLOCK_PROPERTY_ID' == '84']);
        $arMap['MANY'] = new ExpressionField('MANY', "GROUP_CONCAT(DISTINCT %s SEPARATOR ', ')", ['M16.VALUE']);

        return $arMap;
    }
    //пользовательские поля
    public static function getUfId()
    {
        return 'LINUX_SOFT';
    }
}
