<?php

namespace Chazov\Homework;

use Bitrix\Main;
use Bitrix\Main\Entity;

/**
 * Class ElementPropS16Table
 *
 * Fields:
 * <ul>
 * <li> IBLOCK_ELEMENT_ID int mandatory
 * <li> PROPERTY_81 double optional
 * <li> PROPERTY_82 string optional
 * <li> PROPERTY_83 int optional
 * <li> PROPERTY_84 string optional
 * <li> PROPERTY_85 string optional
 * </ul>
 *
 * @package Bitrix\Iblock
 **/
class LinuxSoftS16Table extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_iblock_element_prop_s16';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            new Entity\IntegerField(IBLOCK_ELEMENT_ID, ['primary' => true]),
            /*new Entity\FloatField(PROPERTY_81, ['column_name' => 'ISS']),*/
            new Entity\TextField(GITLINK, ['column_name' => 'PROPERTY_82']),
            /*new Entity\IntegerField(PROPERTY_83, ['column_name' => 'LOGO']),
            new Entity\TextField(PROPERTY_84, ['column_name' => 'LAST_VERSION']),
            new Entity\TextField(PROPERTY_85, ['column_name' => 'ACCESS_ON_OS']),*/
        );
    }
}
