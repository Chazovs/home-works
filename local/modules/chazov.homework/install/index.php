<?php

use Bitrix\Main\EventManager;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Application;
use Bitrix\Main\ModuleManager;
use Chazov\Homework\Event;

Loc::loadMessages(__FILE__);

Class chazov_homework extends CModule
{
    
    public function __construct()
    {
        include(__DIR__ . "/version.php");
        $this->MODULE_ID                     = 'chazov.homework';
        $this->MODULE_VERSION                = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE           = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME                   = Loc::getMessage("CHAZOV_HOMEWORK_MODULE_NAME");
        $this->MODULE_DESCRIPTION            = Loc::getMessage("ACADEMY_D7_MODULE_DESC");
        $this->PARTNER_NAME                  = Loc::getMessage("ACADEMY_D7_PARTNER_NAME");
        $this->PARTNER_URI                   = Loc::getMessage("ACADEMY_D7_PARTNER_URI");
        $this->MODULE_SORT                   = 1;
        $this->SHOW_SUPER_ADMIN_GROUP_RIGHTS = 'Y';
        $this->MODULE_GROUP_RIGHTS           = "Y";
    }
    
    public function installEvents()
    {
        $manager = EventManager::getInstance();
        $manager->registerEventHandler('iblock', 'OnBeforeIBlockElementAdd', $this->MODULE_ID, Event::class, 'eventHandler');
        $manager->registerEventHandler('iblock', 'OnBeforeIBlockElementUpdate', $this->MODULE_ID, Event::class, 'eventHandler');
    }
    
    public function unInstallEvents()
    {
        $manager = EventManager::getInstance();
        $manager->unRegisterEventHandler('iblock', 'OnBeforeIBlockElementAdd', $this->MODULE_ID, Event::class, 'eventHandler');
        $manager->unRegisterEventHandler('iblock', 'OnBeforeIBlockElementAdd', $this->MODULE_ID, Event::class, 'eventHandler');
    }
    
    public function doInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        $this->InstallEvents();
    }
    
    public function doUninstall()
    {
        $this->UnInstallEvents();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }
}
