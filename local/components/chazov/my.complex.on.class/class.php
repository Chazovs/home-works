<?
namespace Bex\Bbc\Components;
use Bex\Bbc\BasisRouter;
use Bitrix\Iblock\Component\Tools;
use Bitrix\Main\Loader;
use Exception;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
if (!Loader::includeModule('bex.bbc')) {
    echo "обломись, нету у тебя bex.bbc";
    return false;
}


class MyComplexOnClass extends BasisRouter
{
    //дефаултный шаблон
    protected $defaultSefPage = 'list';

/*
   public function onPrepareComponentParams($params)
    {
        $this->needModules = array_unique(array_merge($this->needModules, ['iblock']));

        return parent::onPrepareComponentParams($params);
    }

    //не знаю что - видать главная функция стартующая. надо разобраться
    protected function showExceptionUser(Exception $exception)
    {

        $this->show404();
    }*/

    //шаблоны адресов страниц
    protected function setSefDefaultParams()
    {

        $this->defaultUrlTemplates404 = [
            'list' => '',
            'detail' => '#ELEMENT_ID#/',
        ];
        //восстанавливаемые переменные. вроде можно и не указывать, если не меняешь названия? надо уточнить
        $this->componentVariables     = [
            'ELEMENT_ID',
        ];
    }

 /* public function show404()
    {
        Tools::process404(false, true, true, true);
    }*/
}

