<?php

use \Chazov\Homework\LinuxSoftTable;
use \Bitrix\Main\Loader;

Loader::includeModule('chazov.homework');
CModule::IncludeModule("Iblock");

class ClassComponents extends CBitrixComponent
{
    public function executeComponent()
    {
        //домашка по компоенту:
        /*$this->arResult['DATE2'] = CIBlockElement::GetList(
            array(),
            array(
                "IBLOCK_ID" => 16,
            ),
            false,
            false,
            array('NAME', 'ID', 'IBLOCK_ID', 'CODE',  'PREVIEW_TEXT', 'DETAIL_PICTURE')
        );*/
        
        /*$this->arResult['DATE2'] = LinuxSoftTable::getList()->fetchCollection();*/
        
        //домашка по ORM
        $this->arResult['DATE2'] = LinuxSoftTable::getList(
            [
                'select' => ['NAME', 'S16.GITLINK', 'FIO', 'PICTURE', 'MANY', 'UF_LINUX_VERSION', 'UF_HP_DAYS', 'MANY'], // имена полей, которые необходимо получить в результате
                'filter' => ["=IBLOCK_ID" => 16],
                'limit'  => 10,
            ]
        );
        
        /*$this->arResult['DATE4'] = LinuxSoftTable::getList(
            [
                'select' => ['MANY', 'FIO', 'NAME'], // имена полей, которые необходимо получить в результате
                'filter' => ["=IBLOCK_ID" => 16],
                'group'  => ['M16.VALUE'],
                'limit'  => 10,
            ]
        );*/
        
        
        $this->includeComponentTemplate();
    }
}
