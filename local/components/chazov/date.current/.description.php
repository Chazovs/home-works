<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$arComponentDescription = array(
    "NAME" => "Лист на классах",
    "DESCRIPTION" => "Выводим список linuxSoft",
    "PATH" => array(
        "ID" => "dv_class",
        "CHILD" => array(
            "ID" => "onGetList",
            "NAME" => "Лист на GetList"
        )
    ),
    "ICON" => "/images/icon.gif",
);
?>