<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$arComponentDescription = array(
    "NAME" => "Комплексный на классах без BBC",
    "DESCRIPTION" => "на классах",
    "COMPLEX" => "Y",
    "PATH" => array(
        "ID" => "complex_class",
        "CHILD" => array(
            "ID" => "complex.on.class.whithout.bbc",
            "NAME" => "С классами"
        )
    ),
    "ICON" => "/images/icon.gif",
);
