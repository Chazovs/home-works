<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

CModule::IncludeModule("Iblock");

class MyList extends CBitrixComponent
{
    /**
     * @var array
     */
    public $arDefaultUrlTemplates404 = [
        "list" => "",
        "detail" => "#ELEMENT_CODE#",
    ];

    /**
     * @var array
     */
    public $arComponentVariables = ['ELEMENT_CODE'];

    /**
     * @var string
     */
    public $SEF_FOLDER = '';
    public $arVariables = [];
    public $componentPage;

    public function getParameters()
    {
        $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates(
            $this->arDefaultUrlTemplates404,
            $this->arParams['SEF_URL_TEMPLATES']
        );
        //получаем набор восстановленных параметров и название шаблона
        $this->componentPage = CComponentEngine::ParseComponentPath(
            $this->arParams['SEF_FOLDER'],
            $arUrlTemplates,
            $this->arVariables
        );
        if (!$this->componentPage) {
            $this->componentPage = "list";
        }
        $this->SEF_FOLDER = $this->arParams['SEF_FOLDER'];
        $this->arResult = array(
            "FOLDER" => $this->arParams["SEF_FOLDER"],
            "URL_TEMPLATES" => $arUrlTemplates,
            "VARIABLES" => $this->arVariables
        );
    }

    /**
     * Главный метод, подключающий шаблон компонента
     * @return mixed|void
     */
    public function executeComponent()
    {
        $this->getParameters();
        $this->IncludeComponentTemplate($this->componentPage);
    }
}
