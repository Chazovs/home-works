<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<?
$arDefaultUrlTemplates404 = [
    "list" => "",
    "detail" => "#ELEMENT_ID#",
];

$arComponentVariables        = ['ELEMENT_ID'];
$SEF_FOLDER                  = '';

$arVariables = [];

$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates(
    $arDefaultUrlTemplates404,
    $arParams['SEF_URL_TEMPLATES']
);

$arVariableAliases = CComponentEngine::MakeComponentVariableAliases(
    $arDefaultVariableAliases404,
    $arParams['VARIABLE_ALIASES']
);

$componentPage = CComponentEngine::ParseComponentPath(
    $arParams['SEF_FOLDER'],
    $arUrlTemplates,
    $arVariables
);

//сучка, не восстанавливает переменные
echo "<b>componentPage:</b> <pre>";
print_r($componentPage);
echo "</pre><br><br><b>arParams[\"SEF_FOLDER\"]:</b> <pre>";
print_r($arParams["SEF_FOLDER"]);
echo "</pre><br><br><b>arUrlTemplates: </b><pre>";
print_r($arUrlTemplates);
echo "</pre><br><br><b>APPLICATION->GetCurPage():</b><pre>";
print_r($APPLICATION->GetCurPage());
echo "</pre><br><br><b>arVariables:</b><pre>";
print_r($arVariables);
echo "</pre><br><br><b>_REQUEST</b><pre>";
var_dump($arResult['VARIABLES']['ELEMENT_ID']);
echo "</pre>";

if (!$componentPage) {
    $componentPage = "list";
}
$SEF_FOLDER = $arParams['SEF_FOLDER'];
$arResult = array(
    "FOLDER" => $arParams["SEF_FOLDER"],
    "URL_TEMPLATES" => $arUrlTemplates,
    "VARIABLES" => $arVariables
);

$this->IncludeComponentTemplate($componentPage);
