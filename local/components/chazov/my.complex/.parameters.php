<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "TEMPLATE_FOR_DATE" => array(
            "PARENT" => "BASE",
            "NAME" => "Шаблон для даты",
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "DEFAULT" => "Y-m-d",
        ),


        "SEF_MODE" => Array(
            "list" => array(
                "NAME" => "список комплексного",
                "DEFAULT" => "/LinuxSoftComplex/",
                "VARIABLES" => array()
            ),
            "detail" => array(
                "NAME" => "детальная новость комплексного",
                "DEFAULT" => "#ELEMENT_ID#/",
                "VARIABLES" => array("ELEMENT_ID")
            ),
        ),
    ),
);
?>