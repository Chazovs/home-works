<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
CModule::IncludeModule("Iblock");
$arResult['DATE2'] = CIBlockElement::GetList(
    array(),
    array(
        "IBLOCK_ID" => 16,
    ),
    false,
    false,
    array('NAME', 'ID', 'IBLOCK_ID', 'PREVIEW_TEXT', 'DETAIL_PICTURE')
);

//включаем компонент
$this->IncludeComponentTemplate();
