<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
    "NAME" => GetMessage("Текущая дата (просто фигачу па мануалу)"),
    "DESCRIPTION" => GetMessage("Выводим текущую дату"),
    "PATH" => array(
        "ID" => "not-bbc",
        "CHILD" => array(
            "ID" => "first",
            "NAME" => "Текущая дата"
        )
    ),
    "ICON" => "/images/icon.gif",
);
?>