<?
declare(strict_types=1);

interface primat
{
    public function eat($food);

    public function drink();
}

abstract class human
{
    public function goForWork(int $chislo): string
    {
        echo "иду на работую. мое любимое число: " . $chislo;
    }
}

trait plankton
{
    function drinkCoffee(string $like): string
    {
        echo "я пью " . $like . " кофе";
    }
}


class developer extends human implements primat
{
    use plankton;

    // должность разработчика
    var $devStatus;
    // специализация разработчика
    var $devSpec;

    function __construct($devStatus, $devSpec)
    {
        if ($devSpec != "front" && $devSpec != "back") {
            throw new Exception("Специализация может иметь значения только front и back");
        }
        if (gettype($devStatus) != "string") {
            throw new Exception("Должность должна быть строковым значением");
        }
        $this->devSpec = $devSpec;
        $this->devStatus = $devStatus;


    }

    public function eat($food)
    {
        echo "я ем " . $food;
    }

    public function drink()
    {
        echo "я пью";
    }

    public function work()
    {
        echo "моя специализация " . $this->devSpec;
    }
}

try {
    $ivan = new developer("джун", "front");
    $anna = new developer("middle", "back");
    var_dump($ivan);
    echo "<br>";
    var_dump($anna);
} catch (Exception $e) {
    echo "все не норм. А конкретно " . $e->getMessage() . "<br>";
}


//Синглтон
class DB
{

    // для безопасности настройки лучше хранить в файле с конфигом
    private static $host = '127.0.0.1';
    private static $user = 'root';
    private static $pass = '12345';
    private static $db_name = 'warehouse';
    private static $db;

    /**
     * @return mixed
     */
    public static function getConnection()
    {

        if (self::$db === null) {
            $db = new mysqli(self::$host, self:: $user, self::$pass, self::$db_name);
            $db->set_charset('utf8');
            $db->character_set_name();
            $db->query("SET CHARACTER SET utf8");
        }
        return self::$db;
    }

    private function __construct()
    {
    }

    private function __clone()
    {
    }

}

$db = DB::getConnection();
$db2 = DB::getConnection();

var_dump($db == $db2); // bool(true)


//mail("E-mail получателя", "Загаловок", "Текст письма \n 1-ая строчка \n 2-ая строчка \n 3-ая строчка");
