<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Персональный рабочий стол");
?>

<?$APPLICATION->IncludeComponent("bitrix:desktop", "Chazov2", Array(
	"ID" => "holder1",	// Идентификатор
		"CAN_EDIT" => "Y",	// Разрешить настраивать рабочий стол всем авторизованным пользователям
		"COLUMNS" => "3",	// Количество столбцов
		"COLUMN_WIDTH_0" => "33%",	// Размер столбца (px или %) #1
		"COLUMN_WIDTH_1" => "33%",	// Размер столбца (px или %) #2
		"COLUMN_WIDTH_2" => "33%",	// Размер столбца (px или %) #3
		"GADGETS" => array(	// Доступные гаджеты
			0 => "ALL",
		),
		"G_RSSREADER_CACHE_TIME" => "3600",	// Время кеширования, сек (0-не кешировать)
		"G_RSSREADER_SHOW_URL" => "Y",	// Показывать ссылку на подробную информацию
		"G_RSSREADER_PREDEFINED_RSS" => "",	// Предустановленные RSS ленты (каждая с новой строки)
		"GU_RSSREADER_CNT" => "10",	// Количество новостей (0 - показывать все)
		"GU_RSSREADER_RSS_URL" => "",	// Ссылка на RSS ленту
		"G_PROBKI_CACHE_TIME" => "3600",	// Время кеширования, сек (0-не кешировать)
		"G_PROBKI_SHOW_URL" => "Y",	// Показывать ссылку на подробную информацию
		"GU_PROBKI_CITY" => "c213",	// Город
		"G_WEATHER_CACHE_TIME" => "3600",	// Время кеширования, сек (0-не кешировать)
		"G_WEATHER_SHOW_URL" => "Y",	// Показывать ссылку на подробную информацию
		"GU_WEATHER_CITY" => "c213",	// Город
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>